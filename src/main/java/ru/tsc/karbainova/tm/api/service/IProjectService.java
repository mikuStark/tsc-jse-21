package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    void clear(String userId);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project removeById(String userId, String id);

    Project removeByIndex(String userId, Integer index);

    Project removeByName(String userId, String name);

    Project findById(String userId, String id);

    Project findByIndex(String userId, Integer index);

    Project findByName(String userId, String name);

    Project startById(String userId, String id);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);
}
