package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "status-start-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start by id";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
    }
}
