package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;

public class ProjectClearCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "clear-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear project";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }
}
